<?php

/** @var Factory $factory */

use App\Message;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Message::class, function (Faker $faker) {
    return [
        "content" => $faker->realText(500)
    ];
});
