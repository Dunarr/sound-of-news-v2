<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'email_verified_at' => now(),
        'salt' => $salt = $faker->regexify('[A-Za-z0-9]{20}'),
        'token' => $faker->regexify('[A-Za-z0-9]{50}'),
        'plainPassword' => 'password',
        'remember_token' => Str::random(10),
        'renew' => false,
        'active' => $faker->boolean(75),
    ];
});
