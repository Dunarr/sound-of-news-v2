<?php

/** @var Factory $factory */

use App\Report;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Report::class, function (Faker $faker) {
    return [
        "content" => $faker->realText(500)
    ];
});
