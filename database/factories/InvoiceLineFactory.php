<?php

/** @var Factory $factory */

use App\InvoiceLine;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(InvoiceLine::class, function (Faker $faker) {
    return [
        "quantity"=>$faker->numberBetween(1,10),
        "price"=>$faker->randomFloat(2,1,30),
        "item"=>$faker->realText($faker->numberBetween($min = 15, $max = 50)),
    ];
});
