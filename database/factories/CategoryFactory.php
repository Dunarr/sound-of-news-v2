<?php

/** @var Factory $factory */

use App\Category;
use Faker\Generator as Faker;
use Illuminate\Database\Eloquent\Factory;

$factory->define(Category::class, function (Faker $faker) {
    return [
        'title' => $title = $faker->realText($faker->numberBetween($min = 15, $max = 50)),
        'slug' => Str::slug($title)
    ];
});
