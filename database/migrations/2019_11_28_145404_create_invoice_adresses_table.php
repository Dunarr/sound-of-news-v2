<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceAdressesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invoice_adresses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('adress');
            $table->string('adress2')->nullable();
            $table->string('zip_code');
            $table->string('city');
            $table->string('country');
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
            $table->softDeletes();
        });
        Schema::table('invoices', function (Blueprint $table) {
            $table->unsignedBigInteger('invoice_adresse_id')->nullable();
            $table->foreign('invoice_adresse_id')->references('id')->on('invoice_adresses');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('invoices', function (Blueprint $table) {
            $table->dropForeign(['invoice_adresse_id']);
            $table->dropColumn('invoice_adresse_id');
        });
        Schema::dropIfExists('invoice_adresses');
    }
}
