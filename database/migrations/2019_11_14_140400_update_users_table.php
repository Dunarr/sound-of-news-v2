<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('salt')->nullable();
            $table->boolean('renew')->default(0);
            $table->string('token')->nullable();
            $table->boolean('active')->default(0);
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('salt');
            $table->dropColumn('renew');
            $table->dropColumn('token');
            $table->dropColumn('active');
            $table->dropSoftDeletes();
        });
    }
}
