<?php

use App\Subscription;
use App\User;
use Illuminate\Database\Seeder;

class FakeSeeder extends Seeder
{
    public static function GenerateEntities($model, $quantity)
    {
        $entities = [];
        switch ($model):
            case App\Subscription::class:
                $entities = factory(App\Subscription::class, $quantity)->create();
                break;
            case App\Category::class:
                $entities = factory(App\Category::class, $quantity)->create();
                break;
            case App\User::class:
                $entities = factory(App\User::class, $quantity)->create();
                break;
            case App\Thread::class:
                $entities = factory(App\Thread::class, $quantity)->make()->each(function (\App\Thread $thread) {
                    $thread->user()->associate(App\User::inRandomOrder('')->first())->save();
                });
                break;
            case App\Tag::class:
                $entities = factory(App\Tag::class, $quantity)->make()->each(function (App\Tag $tag) {
                    $tag->user()->associate(App\User::inRandomOrder('')->first())->save();
                });
                break;
            case App\Article::class:
                $entities = factory(App\Article::class, $quantity)->make()->each(function (App\Article $article) {
                    $article->category()->associate(App\Category::inRandomOrder('')->first());
                    $article->user()->associate(App\User::inRandomOrder('')->first())->save();
                    $article->tags()->sync(App\Tag::inRandomOrder('')->take(10)->get());
                    $article->save();
                });
                break;
            case App\Comment::class:
                $entities = factory(App\Comment::class, $quantity)->make()->each(function (App\Comment $comment) {
                    $comment->user()->associate(App\User::inRandomOrder('')->first());
                    $comment->article()->associate(App\Article::inRandomOrder('')->first());
                    $comment->save();
                });
                break;
            case App\Message::class:
                $entities = factory(App\Message::class, $quantity)->make()->each(function (App\Message $message) {
                    $message->user()->associate(App\User::inRandomOrder('')->first())->save();
                    $message->thread()->associate(App\Thread::inRandomOrder('')->first())->save();
                });
                break;
            case App\Medium::class:
                $entities = factory(App\Medium::class, $quantity)->create();

                break;
            case App\Report::class:
                $entities = factory(App\Report::class, $quantity)->make()->each(function (App\Report $report) {
                    $report->content()->associate(App\Article::inRandomOrder('')->first())->save();
                });
                break;
            case App\InvoiceAdress::class:
                $entities = factory(App\InvoiceAdress::class, $quantity)->make()->each(function (App\InvoiceAdress $invoiceAdress) {
                    $invoiceAdress->user()->associate(App\User::inRandomOrder('')->first())->save();
                });
                break;
            case App\Invoice::class:
                $entities = factory(App\Invoice::class, $quantity)->make()->each(function (App\Invoice $invoice) {
                    /** @var User $user */
                    $user = App\User::inRandomOrder('')->first();
                    $invoice->user()->associate($user)->save();
                    $invoice->invoiceAdresse()->associate($user->invoiceAdresses()->inRandomOrder('')->first())->save();
                });
                break;
            case App\InvoiceLine::class:
                $entities = factory(App\InvoiceLine::class, $quantity)->make()->each(function (App\InvoiceLine $invoiceLine) {
                    /** @var Subscription $subscription */
                    $subscription = App\Subscription::inRandomOrder('')->first();
                    $invoiceLine->price = $subscription->price;
                    $invoiceLine->item = $subscription->duration . ' - (' . $subscription->price . ')';
                    $invoiceLine->quantity = 1;
                    $invoiceLine->subscription()->associate($subscription)->save();
                    $invoiceLine->invoice()->associate(App\Invoice::inRandomOrder('')->first())->save();
                });
                break;
        endswitch;
        return $quantity === 1?$entities[0]:$entities;
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        self::GenerateEntities(App\Subscription::class, 3);
        self::GenerateEntities(App\Category::class, 10);
        self::GenerateEntities(App\User::class, 10);
        self::GenerateEntities(App\Thread::class, 10);
        self::GenerateEntities(App\Tag::class, 20);
        self::GenerateEntities(App\Article::class, 50);
        self::GenerateEntities(App\Comment::class, 10);
        self::GenerateEntities(App\Message::class, 10);
        self::GenerateEntities(App\Medium::class, 10);
        self::GenerateEntities(App\Report::class, 10);
        self::GenerateEntities(App\InvoiceAdress::class, 10);
        self::GenerateEntities(App\Invoice::class, 10);
        self::GenerateEntities(App\InvoiceLine::class, 10);
    }
}
