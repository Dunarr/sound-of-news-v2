class OpenEditor {
  constructor(input) {
    input.style.display = "none";
    const el = document.createElement("div");
    el.innerHTML = input.value;
    input.parentNode.insertBefore(el, input.nextSibling);
    el.classList = input.classList;
    el.classList.add("open-editor");
    this.listenSelect = false;
    el.contentEditable = true;
    el.addEventListener("mousedown", event => {
      this.listenSelect = event.target
    });
    el.addEventListener("blur", e => {
      input.value = e.target.innerHTML
    });
    this.el = el
    this.generateMenu();
  }

  doAction = action => event => {
    if (action === "h1" || action === "h2" || action === "p") {
      document.execCommand("formatBlock", false, action);
    } else {
      document.execCommand(action, false, null);
    }
    event.preventDefault()
    return false;
  };
  handleSelect = () => {
    if (this.listenSelect) {
      if (window.getSelection().toString().length > 0) {
        this.displayMenu(this.listenSelect)
      }
      this.listenSelect = false
    }
  };

  hideMenu = event => {
    if (!this.menu.contains(event.target)) {
      this.menu.style.display = "none"
    }
  };
  generateMenu = () => {
    this.menu = document.createElement("div");
    this.menu.classList.add("contextual-menu");
    const commands = [
      {
        "action": "bold",
        "icon": "<svg viewBox=\"0 0 24 24\" width=\"24\" height=\"24\" stroke=\"currentColor\" stroke-width=\"2\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"css-i6dzq1\"><path d=\"M6 4h8a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z\"></path><path d=\"M6 12h9a4 4 0 0 1 4 4 4 4 0 0 1-4 4H6z\"></path></svg>"
      },
      {
        "action": "italic",
        "icon": "<svg viewBox=\"0 0 24 24\" width=\"24\" height=\"24\" stroke=\"currentColor\" stroke-width=\"2\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"css-i6dzq1\"><line x1=\"19\" y1=\"4\" x2=\"10\" y2=\"4\"></line><line x1=\"14\" y1=\"20\" x2=\"5\" y2=\"20\"></line><line x1=\"15\" y1=\"4\" x2=\"9\" y2=\"20\"></line></svg>"
      },
      {
        "action": "underline",
        "icon": "<svg viewBox=\"0 0 24 24\" width=\"24\" height=\"24\" stroke=\"currentColor\" stroke-width=\"2\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"css-i6dzq1\"><path d=\"M6 3v7a6 6 0 0 0 6 6 6 6 0 0 0 6-6V3\"></path><line x1=\"4\" y1=\"21\" x2=\"20\" y2=\"21\"></line></svg>"
      },
      {
        "action": "justifyLeft",
        "icon": "<svg viewBox=\"0 0 24 24\" width=\"24\" height=\"24\" stroke=\"currentColor\" stroke-width=\"2\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"css-i6dzq1\"><line x1=\"17\" y1=\"10\" x2=\"3\" y2=\"10\"></line><line x1=\"21\" y1=\"6\" x2=\"3\" y2=\"6\"></line><line x1=\"21\" y1=\"14\" x2=\"3\" y2=\"14\"></line><line x1=\"17\" y1=\"18\" x2=\"3\" y2=\"18\"></line></svg>"
      },
      {
        "action": "justifyCenter",
        "icon": "<svg viewBox=\"0 0 24 24\" width=\"24\" height=\"24\" stroke=\"currentColor\" stroke-width=\"2\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"css-i6dzq1\"><line x1=\"18\" y1=\"10\" x2=\"6\" y2=\"10\"></line><line x1=\"21\" y1=\"6\" x2=\"3\" y2=\"6\"></line><line x1=\"21\" y1=\"14\" x2=\"3\" y2=\"14\"></line><line x1=\"18\" y1=\"18\" x2=\"6\" y2=\"18\"></line></svg>"
      },
      {
        "action": "justifyRight",
        "icon": "<svg viewBox=\"0 0 24 24\" width=\"24\" height=\"24\" stroke=\"currentColor\" stroke-width=\"2\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"css-i6dzq1\"><line x1=\"21\" y1=\"10\" x2=\"7\" y2=\"10\"></line><line x1=\"21\" y1=\"6\" x2=\"3\" y2=\"6\"></line><line x1=\"21\" y1=\"14\" x2=\"3\" y2=\"14\"></line><line x1=\"21\" y1=\"18\" x2=\"7\" y2=\"18\"></line></svg>"
      },
      {
        "action": "justifyFull",
        "icon": "<svg viewBox=\"0 0 24 24\" width=\"24\" height=\"24\" stroke=\"currentColor\" stroke-width=\"2\" fill=\"none\" stroke-linecap=\"round\" stroke-linejoin=\"round\" class=\"css-i6dzq1\"><line x1=\"21\" y1=\"10\" x2=\"3\" y2=\"10\"></line><line x1=\"21\" y1=\"6\" x2=\"3\" y2=\"6\"></line><line x1=\"21\" y1=\"14\" x2=\"3\" y2=\"14\"></line><line x1=\"21\" y1=\"18\" x2=\"3\" y2=\"18\"></line></svg>"
      },
      {
        "action": "p",
        "icon": "P"
      },
      {
        "action": "h1",
        "icon": "H1"
      },
      {
        "action": "h2",
        "icon": "H2"
      },
    ];
    commands.forEach(e => {
      const actionButton = document.createElement("a");
      actionButton.classList.add("action-button");
      actionButton.innerHTML = e.icon;
      actionButton.href = "#";
      actionButton.addEventListener("click", this.doAction(e.action));
      this.menu.append(actionButton)
    });
    this.el.parentNode.insertBefore(this.menu, this.el);
  };

  displayMenu(el) {
    const pos = el.getBoundingClientRect();
    this.menu.style.top = Math.round(pos.top - 5 + document.documentElement.scrollTop) + "px";
    this.menu.style.left = Math.round(pos.left + 15) + "px";
    console.log(pos.top - 5 + document.documentElement.scrollTop);
    this.menu.style.display = "block"
  }
}

function initOpenEditor(selector) {
  const result = [];
  const els = document.querySelectorAll(selector);
  for (let i = 0; i < els.length; i++) {
    result.push(new OpenEditor(els[i]))
  }
  return result
}

initOpenEditor("textarea");
