<?php

use App\Article;
use App\Category;
use App\Invoice;
use App\InvoiceAdress;
use App\InvoiceLine;
use App\Medium;
use App\Message;
use App\Report;
use App\Right;
use App\Role;
use App\Subscription;
use App\Tag;
use App\User;

return [
    "resources" => [
        "articles" => ['model' => Article::class, "label" => "article", "icon" => "file-text",
            "form" => [
                ['type' => "string", 'id' => 'title', 'label' => 'Titre'],
                ['type' => "string", 'id' => 'slug', 'label' => 'Slug'],
                ['type' => "text", 'id' => 'content', 'label' => 'Contenu'],
                ['type' => "choice", 'id' => 'medium_id', 'label' => 'Image', 'class' => Medium::class, 'select-label' => "path", 'multiple' => false, 'gallery' => true],
                ['type' => "choice", 'id' => 'category_id', 'label' => 'Categorie', 'class' => Category::class, 'select-label' => "title", 'multiple' => false],
                ['type' => "choice", 'id' => 'tags', 'label' => 'Tags', 'class' => Tag::class, 'select-label' => "title", 'multiple' => true],
                ['type' => "choice", 'id' => 'user_id', 'label' => 'Auteur', 'class' => User::class, 'select-label' => "name", 'multiple' => false],
                ['type' => "bool", 'id' => 'premium', 'label' => 'Premium'],
                ['type' => "bool", 'id' => 'published', 'label' => 'Publié'],
            ],
            "validator" => [
                'title' => "required|min:10",
                'content' => "required|min:10",
                'category_id' => "required",
            ]
        ],
        "categories" => ['model' => Category::class, "label" => "catégorie", "icon" => "bookmark",
            "form" => [
                ['type' => 'string', 'id' => 'title', 'label' => 'Titre'],
                ['type' => 'string', 'id' => 'slug', 'label' => 'Slug']
            ]
        ],
        "tags" => ['model' => Tag::class, "label" => "tag", "icon" => "tag",
            "form" => [
                ['type' => 'string', 'id' => 'title', 'label' => 'Titre'],
                ['type' => 'string', 'id' => 'slug', 'label' => 'Slug']
            ]
        ],
        "invoices" => ['model' => Invoice::class, "label" => "facture", "icon" => "book",
            "form" => [
                ['type' => 'string', 'id' => 'number', 'label' => 'Numéro de facture'],
                ['type' => "choice", 'id' => 'user_id', 'label' => 'Utilisateur', 'class' => User::class, 'select-label' => "name", 'multiple' => false],
                ['type' => "choice", 'id' => 'invoice_adresse_id', 'label' => 'Adresse', 'class' => InvoiceAdress::class, 'select-label' => "adress", 'multiple' => false],
                ['type' => "choice", 'id' => 'status', 'label' => 'Statut', 'choices' => ['VALIDATED' => 'Validée', 'PROCESSING' => 'En cours', 'CANCELED' => 'Annulée', 'FAILED' => 'Erreur'], 'multiple' => false],
            ]
        ],
        "invoice-lines" => ['model' => InvoiceLine::class, "label" => "lignes de facturation", "icon" => "list",
            "form" => [
                ['type' => "choice", 'id' => 'invoice_id', 'label' => 'Facture', 'class' => Invoice::class, 'select-label' => "number", 'multiple' => false],
                ['type' => "choice", 'id' => 'subscription_id', 'label' => 'Abonnement', 'class' => Subscription::class, 'select-label' => "duration", 'multiple' => false],
            ]
        ],
        "invoice-adresses" => ['model' => InvoiceAdress::class, "label" => "adresse", "icon" => "map-pin",
            "form" => [
                ['type' => "choice", 'id' => 'user_id', 'label' => 'Utilisateur', 'class' => User::class, 'select-label' => "name", 'multiple' => false],
                ['type' => 'string', 'id' => 'adress', 'label' => 'Adresse'],
                ['type' => 'string', 'id' => 'adress2', 'label' => 'Adresse (suite)'],
                ['type' => 'string', 'id' => 'zip_code', 'label' => 'code postal'],
                ['type' => 'string', 'id' => 'city', 'label' => 'Ville'],
                ['type' => 'string', 'id' => 'country', 'label' => 'Pays'],

            ]
        ],
        "reports" => ['model' => Report::class, "label" => "signalement", "icon" => "flag",
            "form" => [
//                ['type' => "choice", 'id' => 'user_id', 'label' => 'Utilisateur', 'class' => Model::class, 'select-label' => "name", 'multiple' => false],
            ]
        ],
        "users" => ['model' => User::class, "label" => "utilisateur", "icon" => "users",
            "form" => [
                ['type' => 'string', 'id' => 'name', 'label' => 'Pseudo'],
                ['type' => 'mail', 'id' => 'email', 'label' => 'email'],
                ['type' => 'pass', 'id' => 'plainPassword', 'label' => 'Mot de passe'],
                ['type' => "choice", 'id' => 'role_id', 'label' => 'Rôle', 'class' => Role::class, 'select-label' => "title", 'multiple' => false],
                ['type' => "bool", 'id' => 'active', 'label' => 'Compte actif'],
            ]
        ],
        "roles" => ['model' => Role::class, "label" => "rôle", "icon" => "user-check",
            "form" => [
                ['type' => 'string', 'id' => 'title', 'label' => 'Titre'],
                ['type' => "choice", 'id' => 'rights', 'label' => 'Droits', 'class' => Right::class, 'select-label' => "name", 'multiple' => true],
            ]
        ],
        "rights" => ['model' => Right::class, "label" => "droit", "icon" => "check-square",
            "form" => [
                ['type' => 'string', 'id' => 'name', 'label' => 'Titre'],
            ]
        ],
        "subscriptions" => ['model' => Subscription::class, "label" => "abonnement", "icon" => "dollar-sign",
            "form" => [
                ['type' => 'string', 'id' => 'price', 'label' => 'Prix (en €)'],
                ['type' => 'string', 'id' => 'duration', 'label' => 'Durée']
            ]
        ],
        "threads" => ['model' => \App\Thread::class, "label" => "thread", "icon" => "edit-3",
            "form" => [
                ['type' => 'string', 'id' => 'title', 'label' => 'Titre'],
                ['type' => 'string', 'id' => 'slug', 'label' => 'Slug'],
                ['type' => "choice", 'id' => 'user_id', 'label' => 'Auteur', 'class' => User::class, 'select-label' => "name", 'multiple' => false],
            ]
        ],
        "messages" => ['model' => Message::class, "label" => "message", "icon" => "message-square",
            "form" => [
                ['type' => "choice", 'id' => 'thread_id', 'label' => 'Thread', 'class' => \App\Thread::class, 'select-label' => "title", 'multiple' => false],
                ['type' => "choice", 'id' => 'user_id', 'label' => 'Auteur', 'class' => User::class, 'select-label' => "name", 'multiple' => false],
                ['type' => "text", 'id' => 'content', 'label' => 'Contenu'],
            ]
        ],
        "medias" => ['model' => Medium::class,"label"=>"media", "icon"=>"image",
            "form" => [
                ['type' => "file", 'id' => 'path', 'label' => 'Fichier à mettre en ligne'],
            ]
        ]
    ]
];
