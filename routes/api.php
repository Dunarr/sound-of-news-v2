<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::prefix('/v1')->group(function () {
    Route::apiResource('/articles','ArticleController');
    Route::apiResource('/categories','CategoryController');
    Route::apiResource('/comments','CommentController');
    Route::apiResource('/invoices','InvoiceController');
    Route::apiResource('/invoice-adresses','InvoiceAdressController');
    Route::apiResource('/invoice-lines','InvoiceLineController');
    Route::apiResource('/media','MediumController');
    Route::apiResource('/messages','MessageController');
    Route::apiResource('/subscriptions','SubscriptionController');
    Route::apiResource('/tags','TagController');
    Route::apiResource('/threads','ThreadController');
    Route::apiResource('/users','UserController');
});
