<?php

namespace Tests\Feature;

use App\Article;
use App\Category;
use App\Tag;
use App\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\CreatesApplication;
use Tests\TestCase;

class ArticleTest extends TestCase
{
    use DatabaseMigrations;


    /**
     * A basic test example.
     *
     * @return void
     */

    public function testArticleCreation() {
        $count = Article::count();
        $this->post('/api/v1/articles', [
            'title' => "my test article",
            'content' => "lorem ipsum dolor sit amet",
            'premium' => false,
            'published' => true,
            'category_id'=>Category::inRandomOrder('')->first()->id,
            'user_id'=>User::inRandomOrder('')->first()->id,
        ]);
        $this->assertEquals($count + 1, Article::count());
    }

    public function testArticlePublication()
    {
        $data = $this->getJson("/api/v1/articles");
        $total = $data->json("meta.total");
        $article = \FakeSeeder::GenerateEntities(Article::class, 1);
        $article->published = false;
        $article->save();
        $article = \FakeSeeder::GenerateEntities(Article::class, 1);
        $article->published = true;
        $article->save();
        $data = $this->get("/api/v1/articles");
        $this->assertLessThan(Article::count(), $data->json("meta.total"));
        $this->assertEquals($total+1, $data->json("meta.total"));
    }

    public function testArticleSlug()
    {
        $article = \FakeSeeder::GenerateEntities(Article::class, 1);
        $articleSlug = $article->slug;
        $this->assertEquals($articleSlug, \Str::slug($article->title), 'slug is generated');
        $article->title = 'new title';
        $this->assertEquals($articleSlug.'-', $article->slug, 'slug is not overwritten');
    }
}
