<?php

namespace Tests\Feature;

use App\Invoice;
use App\InvoiceLine;
use App\Role;
use App\Subscription;
use App\User;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testPremium()
    {
        /**
         * @var $user User
         */
        $user = factory(User::class)->create();
        $this->assertFalse($user->premium);

        $roleAdmin = Role::findByName('Administrator');
        $user->role()->associate($roleAdmin);
        $this->assertTrue($user->premium);

        $user->role()->associate(null);
        $subscription = Subscription::create([
            'price' => 1,
            'duration' => '1 month'
        ]);
        $subscription->save();
        $invoiceLine = factory(InvoiceLine::class)->create();
        $invoiceLine->subscription()->associate($subscription);
        /** @var Invoice $invoice */
        $invoice = factory(Invoice::class)->create();
        $invoice->user()->associate($user);
        $invoice->status = Invoice::VALIDATED;
        $invoice->save();
        $invoiceLine->invoice()->associate($invoice);
        $invoiceLine->save();
        $user->subscription()->associate($subscription);
        $user->save();
        $this->assertInstanceOf(Carbon::class, $user->premium);

        $invoice->created_at = Carbon::now()->sub('2 months');
        $invoice->save();
        $this->assertFalse($user->premium);
    }
}
