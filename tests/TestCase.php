<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;


    public function __construct()
    {
        parent::__construct();
        array_push($this->afterApplicationCreatedCallbacks, function () {
            $this->artisan("migrate:fresh --seed");
            $this->artisan("db:seed --class=FakeSeeder");
        });
    }
}
