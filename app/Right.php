<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Right
 *
 * @property int $id
 * @property string $name
 * @property string|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Role[] $roles
 * @property-read int|null $roles_count
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Right whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Right extends Model
{
    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function __toString()
    {
        return $this->name;
    }
}
