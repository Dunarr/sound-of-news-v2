<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Invoice
 *
 * @property int $id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string $number
 * @property float|null $total_price
 * @property int|null $total_quantity
 * @property int|null $user_id
 * @property string $status
 * @property string|null $deleted_at
 * @property int|null $invoice_adresse_id
 * @property-read \App\InvoiceAdress $adress
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\InvoiceLine[] $lines
 * @property-read int|null $lines_count
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereInvoiceAdresseId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereTotalPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereTotalQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Invoice whereUserId($value)
 * @mixin \Eloquent
 */
class Invoice extends Model
{
    const VALIDATED = "VALIDATED";
    const CANCELED = "CANCELED";
    const FAILED = "FAILED";
    const OPENED = "OPENED";
    protected $fillable = ['number', 'total_price', 'total_quantity', 'status'];

    public function user() {
        return $this->belongsTo(User::class);
    }
    public function invoiceAdresse(){
        return $this->belongsTo(InvoiceAdress::class);
    }
    public function lines(){
        return $this->hasMany(InvoiceLine::class);
    }

    public function __toString()
    {
        return $this->number;
    }
}
