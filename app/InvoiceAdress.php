<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * App\InvoiceAdress
 *
 * @property int $id
 * @property string $adress
 * @property string|null $adress2
 * @property string $zip_code
 * @property string $city
 * @property string $country
 * @property int|null $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Invoice[] $invoices
 * @property-read int|null $invoices_count
 * @property-read \App\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress whereAdress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress whereAdress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress whereUserId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\InvoiceAdress whereZipCode($value)
 * @mixin \Eloquent
 */
class InvoiceAdress extends Model
{
    protected $fillable = ['adress','adress2','zip_code','city','country'];
    public function user() {
        return $this->belongsTo(User::class);
    }
    public function invoices(){
        return $this->hasMany(Invoice::class);
    }
    public function getFullAdressAttribute() {
        $fullAdress = [$this->adress];
        if(!empty($this->adress2)){
            $fullAdress[]=$this->adress2;
        }
        if(!empty($this->city)){
            $fullAdress[]=$this->city;
        }
        if(!empty($this->zip_code)){
            $fullAdress[]=$this->zip_code;
        }
        return join(", ", $fullAdress);
    }

    public function __toString()
    {
        return $this->adress.', '.$this->city;
    }
}
