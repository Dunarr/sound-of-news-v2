<?php

namespace App;

use Eloquent;
use Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\DatabaseNotification;
use Illuminate\Notifications\DatabaseNotificationCollection;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;

/**
 * App\User
 *
 * @property int $id
 * @property string $name
 * @property string $email
 * @property Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property string|null $salt
 * @property int $renew
 * @property string|null $token
 * @property int $active
 * @property string|null $deleted_at
 * @property int|null $role_id
 * @property int|null $subscription_id
 * @property-read Collection|Article[] $articles
 * @property-read int|null $articles_count
 * @property-read Role|null $role
 * @property-read DatabaseNotificationCollection|DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-write mixed $plain_password
 * @property-read Subscription|null $subscription
 * @property-read Collection|Tag[] $tags
 * @property-read int|null $tags_count
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @method static Builder|User whereActive($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereDeletedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User whereName($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereRenew($value)
 * @method static Builder|User whereRoleId($value)
 * @method static Builder|User whereSalt($value)
 * @method static Builder|User whereSubscriptionId($value)
 * @method static Builder|User whereToken($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @mixin Eloquent
 */
class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function articles()
    {
        return $this->belongsToMany(Article::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function subscription()
    {
        return $this->belongsTo(Subscription::class);
    }

    public function setPlainPasswordAttribute($password)
    {
        $this->attributes['salt'] = $salt = substr(str_shuffle(str_repeat($x = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(50 / strlen($x)))), 1, 50);
        $this->attributes['password'] = Hash::make($password);
    }

    public function getRoleAttribute()
    {
        return $this->role_id ? $this->role()->first() : Role::where('title', '=', 'User')->first();
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function hasRight($right)
    {
        return $this->role->hasRight($right);
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getPremiumAttribute()
    {
        if($this->hasRight("always_premium"))
            return true;
        $subscription = $this->subscription;
        if(!$subscription)
            return false;
        $invoice = $this->invoices()
            ->rightJoin('invoice_lines', 'invoices.id', '=', 'invoice_lines.invoice_id')
            ->where('invoice_lines.subscription_id', '=', $subscription->id)
            ->where('invoices.status', '=', Invoice::VALIDATED)
            ->orderBy('invoices.created_at', 'DESC')
            ->select("invoices.created_at")
            ->first();
        if(!$invoice)
            return false;
        $now = Carbon::now();
        $subscriptionStart = $invoice->created_at;
        $subscriptionEnd = $subscriptionStart->add($subscription->duration);
        return $subscriptionEnd->greaterThan($now)?$subscriptionEnd:false;
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class);
    }

    public function invoiceAdresses()
    {
        return $this->hasMany(InvoiceAdress::class);
    }
}
