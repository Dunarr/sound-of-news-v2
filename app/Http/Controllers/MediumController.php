<?php

namespace App\Http\Controllers;

use App\Medium;
use App\Http\Resources\Medium\MediumCollection;
use App\Http\Resources\Medium\MediumRessource;
use Illuminate\Http\Request;

class MediumController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new MediumCollection(Medium::paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $medium = Medium::create($request->all());
        return new MediumRessource($medium);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Medium  $medium
     * @return \Illuminate\Http\Response
     */
    public function show(Medium $medium)
    {
        return new MediumRessource($medium);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Medium  $medium
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Medium $medium)
    {
        $medium->update($request->all());
        return new MediumRessource($medium);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Medium $medium
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Medium $medium)
    {
//        $medium->delete();
        return response(null,200)->json(["success" => true]);
    }
}
