<?php

namespace App\Http\Controllers;

use App\InvoiceAdress;
use App\Http\Resources\InvoiceAdress\InvoiceAdressCollection;
use App\Http\Resources\InvoiceAdress\InvoiceAdressRessource;
use Illuminate\Http\Request;

class InvoiceAdressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new InvoiceAdressCollection(InvoiceAdress::paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $invoiceAdress = InvoiceAdress::create($request->all());
        return new InvoiceAdressRessource($invoiceAdress);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvoiceAdress  $invoiceAdress
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceAdress $invoiceAdress)
    {
        return new InvoiceAdressRessource($invoiceAdress);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvoiceAdress  $invoiceAdress
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoiceAdress $invoiceAdress)
    {
        $invoiceAdress->update($request->all());
        return new InvoiceAdressRessource($invoiceAdress);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\InvoiceAdress $invoiceAdress
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(InvoiceAdress $invoiceAdress)
    {
//        $invoiceAdress->delete();
        return response(null,200)->json(["success" => true]);
    }
}
