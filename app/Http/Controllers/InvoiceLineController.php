<?php

namespace App\Http\Controllers;

use App\InvoiceLine;
use App\Http\Resources\InvoiceLine\InvoiceLineCollection;
use App\Http\Resources\InvoiceLine\InvoiceLineRessource;
use Illuminate\Http\Request;

class InvoiceLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new InvoiceLineCollection(InvoiceLine::paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $invoiceLine = InvoiceLine::create($request->all());
        return new InvoiceLineRessource($invoiceLine);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InvoiceLine  $invoiceLine
     * @return \Illuminate\Http\Response
     */
    public function show(InvoiceLine $invoiceLine)
    {
        return new InvoiceLineRessource($invoiceLine);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\InvoiceLine  $invoiceLine
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, InvoiceLine $invoiceLine)
    {
        $invoiceLine->update($request->all());
        return new InvoiceLineRessource($invoiceLine);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\InvoiceLine $invoiceLine
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(InvoiceLine $invoiceLine)
    {
//        $invoiceLine->delete();
        return response(null,200)->json(["success" => true]);
    }
}
