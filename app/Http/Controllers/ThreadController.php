<?php

namespace App\Http\Controllers;

use App\Thread;
use App\Http\Resources\Thread\ThreadCollection;
use App\Http\Resources\Thread\ThreadRessource;
use Illuminate\Http\Request;

class ThreadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return new ThreadCollection(Thread::paginate(5));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $thread = Thread::create($request->all());
        return new ThreadRessource($thread);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function show(Thread $thread)
    {
        return new ThreadRessource($thread);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Thread  $thread
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Thread $thread)
    {
        $thread->update($request->all());
        return new ThreadRessource($thread);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Thread $thread
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function destroy(Thread $thread)
    {
//        $thread->delete();
        return response(null,200)->json(["success" => true]);
    }
}
