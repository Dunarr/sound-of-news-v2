<?php

namespace App\Http\Resources\Tag;

use App\Http\Resources\Article\ArticleCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class TagRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'articles' => new ArticleCollection($this->articles),
        ];
    }
}
