<?php

namespace App\Http\Resources\Message;

use App\Http\Resources\User\UserRessource;
use App\Http\Resources\User\UserTeaserRessource;
use Illuminate\Http\Resources\Json\JsonResource;

class MessageTeaserRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'author' => new UserTeaserRessource($this->user),
            'created_at' => $this->created_at,
            '@link' => route('messages.show', $this)
        ];
    }
}
