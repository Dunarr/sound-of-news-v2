<?php

namespace App\Http\Resources\Article;

use App\Http\Resources\Category\CategoryCollection;
use App\Http\Resources\Category\CategoryTeaserRessource;
use App\Http\Resources\Comment\CommentCollection;
use App\Http\Resources\Medium\MediumTeaserRessource;
use App\Http\Resources\Tag\TagCollection;
use App\Http\Resources\User\UserTeaserRessource;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'content' => $this->content,
            'premium' => $this->premium,
            'author' => new UserTeaserRessource($this->user),
            'post_date' => $this->created_at,
            'category' => new CategoryTeaserRessource($this->category),
            'thumbnail' => new MediumTeaserRessource($this->medium),
            'tags' => new TagCollection($this->tags),
            'comments' => new CommentCollection($this->comments),
        ];
    }
}
