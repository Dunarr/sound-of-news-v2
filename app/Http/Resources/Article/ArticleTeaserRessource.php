<?php

namespace App\Http\Resources\Article;

use App\Http\Resources\Medium\MediumTeaserRessource;
use Illuminate\Http\Resources\Json\JsonResource;

class ArticleTeaserRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'premium' => $this->premium,
            'thumbnail' => new MediumTeaserRessource($this->medium),
            '@link' => route('articles.show', $this)
        ];
    }
}
