<?php

namespace App\Http\Resources\Invoice;

use App\Http\Resources\InvoiceAdress\InvoiceAdressTeaserRessource;
use App\Http\Resources\InvoiceLine\InvoiceLineCollection;
use App\Http\Resources\User\UserTeaserRessource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'created_at' => $this->created_at,
            'number' => $this->number,
            'total' => $this->total_price,
            'status' => $this->status,
            'content' => $this->content,
            'total_quantity' => $this->total_quantity,
            'owner' => new UserTeaserRessource($this->user),
            'adress' => new InvoiceAdressTeaserRessource($this->invoiceAdresse),
            'lines' => new InvoiceLineCollection($this->lines),
        ];
    }
}
