<?php

namespace App\Http\Resources\Comment;

use App\Http\Resources\User\UserCollection;
use App\Http\Resources\User\UserTeaserRessource;
use Illuminate\Http\Resources\Json\JsonResource;

class CommentTeaserRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'content' => $this->content,
            'author' => new UserTeaserRessource($this->user),
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            '@link' => route('comments.show', $this)
        ];
    }
}
