<?php

namespace App\Http\Resources\InvoiceAdress;

use App\Http\Resources\User\UserTeaserRessource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceAdressRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'full_adress' => $this->fullAdress,
            'line_1' => $this->adress,
            'line_2' => $this->adress2,
            'zip_code' => $this->zip_code,
            'country' => $this->country,
            'city' => $this->city,
            'owner' => new UserTeaserRessource($this->user_id),
            'created_at' => $this->created_at,
        ];
    }
}
