<?php

namespace App\Http\Resources\InvoiceAdress;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InvoiceAdressCollection extends ResourceCollection
{
    public $collects = InvoiceAdressTeaserRessource::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'meta' => [
                'api_version' => 1,
            ],
        ];
    }
}
