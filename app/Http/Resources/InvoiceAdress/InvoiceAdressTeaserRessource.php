<?php

namespace App\Http\Resources\InvoiceAdress;

use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceAdressTeaserRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'adress' => $this->fullAdress,
            '@link' => route('invoice-adresses.show', $this)
        ];
    }
}
