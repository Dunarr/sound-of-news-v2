<?php

namespace App\Http\Resources\Thread;

use App\Http\Resources\Message\MessageCollection;
use App\Http\Resources\User\UserTeaserRessource;
use Illuminate\Http\Resources\Json\JsonResource;

class ThreadRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'creator' => new UserTeaserRessource($this->user),
            'messages' => new MessageCollection($this->messages),
            'created_at' => $this->created_at,
        ];
    }
}
