<?php

namespace App\Http\Resources\Medium;

use Illuminate\Http\Resources\Json\ResourceCollection;

class MediumCollection extends ResourceCollection
{
    public $collects = MediumTeaserRessource::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'meta' => [
                'api_version' => 1,
            ],
        ];
    }
}
