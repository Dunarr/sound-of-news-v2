<?php

namespace App\Http\Resources\Medium;

use Illuminate\Http\Resources\Json\JsonResource;

class MediumTeaserRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'path' => $this->path,
            '@link' => route('media.show', $this)
        ];
    }
}
