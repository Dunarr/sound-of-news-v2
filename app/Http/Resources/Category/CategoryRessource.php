<?php

namespace App\Http\Resources\Category;

use App\Http\Resources\Article\ArticleCollection;
use Illuminate\Http\Resources\Json\JsonResource;

class CategoryRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'slug' => $this->slug,
            'articles' => new ArticleCollection($this->articles),
            'content' => $this->content
        ];
    }
}
