<?php

namespace App\Http\Resources\InvoiceLine;

use App\Http\Resources\Invoice\InvoiceTeaserRessource;
use App\Http\Resources\Subscription\SubscriptionTeaserRessource;
use App\Http\Resources\User\UserTeaserRessource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceLineRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->price,
            'title' => $this->item,
            'quantity' => $this->quantity,
            'product' => new SubscriptionTeaserRessource($this->subscription),
            'invoice' => new InvoiceTeaserRessource($this->invoice),
        ];
    }
}
