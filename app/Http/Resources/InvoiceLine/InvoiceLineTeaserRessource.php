<?php

namespace App\Http\Resources\InvoiceLine;

use App\Http\Resources\Subscription\SubscriptionTeaserRessource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceLineTeaserRessource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'price' => $this->price,
            'title' => $this->item,
            'quantity' => $this->quantity,
            'product' => new SubscriptionTeaserRessource($this->subscription),
            '@link' => route('invoice-lines.show', $this)
        ];
    }
}
