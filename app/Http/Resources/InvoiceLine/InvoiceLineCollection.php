<?php

namespace App\Http\Resources\InvoiceLine;

use Illuminate\Http\Resources\Json\ResourceCollection;

class InvoiceLineCollection extends ResourceCollection
{
    public $collects = InvoiceLineTeaserRessource::class;
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,
            'meta' => [
                'api_version' => 1,
            ],
        ];
    }
}
