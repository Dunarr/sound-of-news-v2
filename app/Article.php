<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

/**
 * App\Article
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property int $premium
 * @property string $content
 * @property int $published
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $deleted_at
 * @property int $category_id
 * @property int $medium_id
 * @property-read \App\Category $category
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Comment[] $comments
 * @property-read int|null $comments_count
 * @property-read \App\Medium $media
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Tag[] $tags
 * @property-read int|null $tags_count
 * @property-read \App\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereCategoryId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereContent($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereDeletedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereMediumId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article wherePremium($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article wherePublished($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereSlug($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Article whereUserId($value)
 * @mixin \Eloquent
 */
class Article extends Model
{
    protected $fillable = ['title', 'slug', 'content', 'premium', 'published', 'category_id', 'user_id', 'medium_id'];
    protected $attributes = array(
        'premium' => false,
        'published' => false
    );
    public function medium()
    {
        return $this->belongsTo(Medium::class);
    }
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class);
    }

    public function setTitleAttribute($title)
    {
        $this->attributes['title'] = $title;
        if (empty($this->attributes['slug'])) {
            $this->attributes['slug'] = \Str::slug($title);
        }
    }

    public
    function setSlugAttribute($slug)
    {
        if (!empty($slug))$this->attributes['slug'] = \Str::slug($slug);


    }

    public function __toString()
    {
        return $this->title;
    }

    public function comments(){
        return $this->hasMany(Comment::class);
    }
    public static function public(){
        return Article::where(['published' => true]);
    }
}
